from django.core.management.base import BaseCommand, CommandError
from urllib.request import urlopen

import json

from events.models import Team

class Command(BaseCommand):
    '''
    This command will be used in our API view to make sure that there is
    up-to-date information for the event App

    This command retrieves the information about teams in the league.

    If this project was going to be deployed to a server this command would be
    added to a cron job prevent calling a command in a view
    '''

    help = "Uses an NFL API to update/create the teams stored in the database"

    def add_arguments(self, parser):
        pass


    def handle(self, *args, **kwargs):

        # URL for the API
        URL = 'https://delivery.chalk247.com/team_rankings/NFL.json?api_key=74db8efa2a6db279393b433d97c2bc843f8e32b0'

        try:
            # Getting the data from the team ranking API
            data = json.load(urlopen(URL))
            data = data['results']['data']

        except Exception as e:
            print('Error: Problem capturing data from the API: ', e)

        try:
            # Using the data to update/create teams
            for item in data:

                # Capturing the data into variables
                team_id = item['team_id']
                name = item['team']
                rank = item['rank']
                last_rank = item['last_week']
                points = item['points']
                modifier = item['modifier']
                adjusted_points = item['adjusted_points']

                # Creating / updating the Team objects
                # If there is a Team object with the team_id and team_name it is updated
                # Else a new object is created
                team, new = Team.objects.update_or_create(

                    rank = rank,
                    last_week = last_rank,
                    points = points,
                    modifier = modifier,
                    adjusted_points = adjusted_points,
                    defaults = {
                        'team_id': team_id,
                        'team': name,
                    },
                )

                # Logging feedback for user on console
                if new:
                    print('Successfully created a new Team object')
                else:
                    print('Successfully updated Team')


        except Exception as e:
            print('Error: Problem creating/updating a team object: ', e)
