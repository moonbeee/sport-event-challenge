from django.core.management.base import BaseCommand, CommandError
from urllib.request import urlopen
from datetime import datetime

import json

from events.models import Event

class Command(BaseCommand):
    '''
    This command will be used in our API view to make sure that there is
    up-to-date information for the event App

    This command retrieves the information on events from an API based on a date range.
    The date range cannot exceed 7 days

    If this project was going to be deployed to a server this command would be
    added to a cron job rather than calling a command in a view
    '''

    help = "Uses an NFL API to update/create the teams stored in the database"

    def add_arguments(self, parser):
        parser.add_argument('start_date', nargs="+", type=str)
        parser.add_argument('end_date', nargs="+", type=str)



    def handle(self, *args, **kwargs):

        try:
            # Parsing command line arguements
            start_date = datetime.strptime(kwargs['start_date'][0], '%Y-%m-%d')
            end_date = datetime.strptime(kwargs['end_date'][0], '%Y-%m-%d')

        except Exception as e:
            print('Error: Improper dates provided via arguments: ', e)

        # Ensuring that the difference between the dates is no more than 7 days
        if abs((start_date - end_date).days) <= 7:

            URL = 'https://delivery.chalk247.com/scoreboard/NFL/%s/%s.json?api_key=74db8efa2a6db279393b433d97c2bc843f8e32b0' % (start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'))

            # Retrieving the data from the API
            try:
                data = json.load(urlopen(URL))
                data = data['results']


            except Exception as e:
                print('Error: could not retrieve data from the API: ', e)

            # try:
                # Looping over the data set that is coming back from the API
                # The API returns an ojbect ofthe day that holds the events for that day

            for day in data:
                if 'data' in data[day]:
                    for id in data[day]['data']:
                        event = data[day]['data'][id]

                        event_id = event['event_id']
                        # Splitting the provided date and time
                        event_datetime = event['event_date']
                        event_date = event_datetime.split()[0]
                        event_time = event_datetime.split()[1]
                        away_team_id = event['away_team_id']
                        away_nick_name = event['away_nick_name']
                        away_city = event['away_city']
                        home_team_id = event['home_team_id']
                        home_nick_name = event['home_nick_name']
                        home_city = event['home_city']

                        # Creating / updating the Event objects
                        # If there is a Team object with the event_id it is updated
                        # Else a new object is created
                        event_record, new = Event.objects.update_or_create(
                            event_date = event_date,
                            event_time = event_time,
                            away_team_id = away_team_id,
                            away_nick_name = away_nick_name,
                            away_city = away_city,
                            home_team_id = home_team_id,
                            home_nick_name = home_nick_name,
                            home_city = home_city,
                            defaults = {
                                'event_id': event_id
                            }
                        )

                        # Logging feedback for user on console
                        if new:
                            print('Successfully created a new Event object')
                        else:
                            print('Successfully updated Event')

            # except Exception as e:
            #     print('Error: Problem creating/updating a Event object')

        else:
            print('Dates provided are out of the 7 day range.')
