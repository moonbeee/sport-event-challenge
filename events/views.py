from django.shortcuts import render
from django.http import JsonResponse
from django.core.management import call_command

from datetime import datetime

from events.models import Event, Team

# Views

def events_by_range(request, start, end):
    '''
    View that handles the request for a JSON response of data on events
    Specifies 2 dates to determine which events to return and errors if the
    dates supplied at more than 7 day span
    '''

    data = []
    teams = {}

    # Checking to see if the dates provided are within the 7 day range
    start_date = datetime.strptime(start, "%Y-%m-%d")
    end_date = datetime.strptime(end, '%Y-%m-%d')

    # Returning an error message via Json if the range is > 7
    if abs((start_date - end_date).days) > 7:
        data.append({'error': 'Dates provided exceed the 7 day limit'})
        return JsonResponse(data, safe=False)
    else:
        # Calling the commands to populate the database with the dates provided.
        # If this App was going to be on a server these commands would not be placed in this view
        # The commands would be called by a cron to get the newest data rather than every time
        # someone hit our end point. This allows serving up already stored data if the supplying
        # API goes down
        call_command('update_create_teams')
        call_command('retrieve_events', start, end)

        # Retrieving the needed events for the
        events = Event.objects.filter(event_date__gte=start, event_date__lte=end)

        # Looping over the QuerySet to create the objects for JsonResponse
        for event in events:
            current = {}

            current['event_id'] = str(event.event_id)
            # Reformatting the datetime to meet the API requirements
            event_date = datetime.strftime(event.event_date, '%d-%m-%Y')
            current['event_date'] = event_date
            current['event_time'] = str(event.event_time)[0:5]
            current['away_team_id']  = str(event.away_team_id)
            current['away_nick_name'] = event.away_nick_name
            current['away_city'] = event.away_city

            # Check to see if the team is already added to the 'teams' dictionary
            # This will save calls to the DB if the object was already used
            if event.away_team_id in teams:
                current['away_rank'] = str(teams[event.away_team_id]['rank'])
                current['away_rank_points'] = str(round(teams[event.away_team_id]['rank_points'], 2))
            # If not adding the team object to the dictionary
            else:
                team = Team.objects.get(team_id=event.away_team_id)
                teams[event.away_team_id] = {}
                teams[event.away_team_id]['rank'] = team.rank
                teams[event.away_team_id]['rank_points'] = team.adjusted_points
                current['away_rank'] = str(team.rank)
                current['away_rank_points'] = str(round(team.adjusted_points, 2))

            current['home_team_id'] = str(event.home_team_id)
            current['home_nick_name'] = event.home_nick_name
            current['home_city'] = event.home_city

            # Check to see if the team is already added to the 'teams' dictionary
            # This will save calls to the DB if the object was already used
            if event.home_team_id in teams:
                current['away_rank'] = str(teams[event.home_team_id]['rank'])
                current['away_rank_points'] = str(round(teams[event.home_team_id]['rank_points'], 2))

            # If not adding the team object to the dictionary
            else:
                team = Team.objects.get(team_id=event.home_team_id)
                teams[event.home_team_id] = {}
                teams[event.home_team_id]['rank'] = team.rank
                teams[event.home_team_id]['rank_points'] = team.adjusted_points
                current['home_rank'] = str(team.rank)
                current['home_rank_points'] = str(round(team.adjusted_points, 2))

            data.append(current)

    return JsonResponse(data, safe=False)
