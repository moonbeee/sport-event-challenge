from django.db import models

class Team(models.Model):
    '''
    Model to hold team data
    Will query these objects when creating the API calls for the events
    '''

    unique_together = ('team_id', 'team')

    team_id = models.IntegerField(null=False, blank=False, unique=True)
    team = models.TextField(max_length=50)
    rank = models.IntegerField()
    last_week = models.IntegerField()
    points = models.DecimalField(max_digits=6, decimal_places=3)
    modifier = models.DecimalField(max_digits=6, decimal_places=3)
    adjusted_points = models.DecimalField(max_digits=6, decimal_places=3)



class Event(models.Model):
    '''
    Model to hold data about events
    Will query these objects when creating the API calls for the events
    '''

    event_id = models.IntegerField(null=False, blank=False, unique=True)
    event_date = models.DateField()
    event_time = models.TimeField()
    away_team_id = models.IntegerField(null=False, blank=False)
    away_nick_name = models.TextField(max_length=50)
    away_city = models.TextField(max_length=50)
    home_team_id = models.IntegerField(null=False, blank=False)
    home_nick_name = models.TextField(max_length=50)
    home_city = models.TextField(max_length=50)
